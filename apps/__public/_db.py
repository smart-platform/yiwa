# coding: utf-8

""""""

from yiwa._io import scan_apps
from yiwa.db import DataConveyor


def refresh_command():
    """更新指令"""
    _apps, _commands = scan_apps()
    DataConveyor().refresh(_apps, _commands)
